/*
 * Licensed over WTFPL<wtfpl.net>
 * Nikolaev Dmitry, 2017.12.10
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 * 
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

#include "python_funcs.h"

const char *get(const char*);

const struct string {
	char *ptr;
	size_t len;
};

char *getUpdates() {
	char url[31337];
	sprintf(url, "https://api.telegram.org/bot%s/getUpdates?offset=%d", g_Token, g_Offset);
	return get(url);
}

void init_string(struct string *s) {
	s->len = 0;
	s->ptr = malloc(s->len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s) {
	size_t new_len = s->len + size*nmemb;
	s->ptr = realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}

const char *get(const char* url) {
	CURL *curl;
	struct string s;
	init_string(&s);
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		return s.ptr;
	}
	return "err";
}

bool sendMessage(char *text, int id) {
	char url[228];
	sprintf(url, "https://api.telegram.org/bot%s/sendMessage?chat_id=%d&text=%s", g_Token, id, text);
	get(url);
	return true; // fixme
}

inline static PyObject* sendMessageHandler(PyObject *self, PyObject *pyArgs) {
	char *text;
	int id;
	PyArg_ParseTuple(pyArgs, "si", &text, &id);
	sendMessage(text, id);
	return python_string("sent\n");
}

inline static PyObject* GetMessages(PyObject *self, PyObject *args) {
	//printf("%s\n", getUpdates());
	return python_string(getUpdates());
}

inline static PyObject* init(PyObject *self, PyObject *pyArgs) {
	char *tkn;
	PyArg_ParseTuple(pyArgs, "s", &tkn);
	strcpy(g_Token, tkn);
	//printf("%s\n", g_Token);
	return python_string(g_Token);
}
//setOffset
inline static PyObject* setOffset(PyObject *self, PyObject *pyArgs) {
	PyArg_ParseTuple(pyArgs, "i", &g_Offset);
	return python_string("ok");
}

static PyMethodDef tgbotMethods[] = {
	{
		"get_messages",
		GetMessages,
		METH_NOARGS,
		"Print 'hello world' from a method defined in a C extension."
	},
	{
		"send_message",
		sendMessageHandler,
		METH_VARARGS,
		"Documentation? Sounds like a joke"
	},
	{
		"init",
		init,
		METH_VARARGS,
		"Documentation? Sounds like a joke"
	},
	{
		"set_offset",
		setOffset,
		METH_VARARGS,
		"Documentation? Sounds like a joke"
	},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef tgbotDefine = {
	PyModuleDef_HEAD_INIT,
	"tgbot",
	"Why do you reading this?..",
	-1,
	tgbotMethods
};

PyMODINIT_FUNC PyInit_tgbot(void) {
	Py_Initialize();
	return PyModule_Create(&tgbotDefine);
}
