#!/usr/bin/python3
from distutils.core import setup, Extension
import sysconfig

extra_compile_args = sysconfig.get_config_var('CFLAGS').split()
extra_compile_args += ['-lcurl']
setup(name='tgbot', version='1.0', ext_modules=[Extension(
  'tgbot', 
  ['main.c'],
  extra_link_args = ["-lcurl"]
)])
