#!/usr/bin/python3
import tgbot, json, time, datetime
#no time to refactoring onMessageGet

class Message:
	def __init__(self, text, id):
		self.text = text
		self.id = id
class Bot:
	def __init__(self, token):
		tgbot.init(token)
	def setOffset(self, offset):
		tgbot.set_offset(offset)
	def send(self, text, id):
		tgbot.send_message(text, id)
	def get(self):
		return tgbot.get_messages()
	def handle(self, callback):
		while True:
			j = json.loads(self.get())
			if j["ok"] == True and j["result"] != []:
				for msg in j["result"]:
					callback(Message(
						msg["message"]["text"],
						msg["message"]["from"]["id"]))
				self.setOffset(j["result"][-1]["update_id"] + 1)
	def onMessageGet(self, msg):
		msg.text = msg.text.replace("бот", "ты") # троллируем пользователя, если он, скорее всего, троллирует нас
		command = msg.text.split(" ")[0].lower()
		# https://navopros.ru/russkij-yazyk/skolko-maternyh-slov
		kurwas = (b'\xd0\xb5\xd0\xb1\xd0\xb0\xd1\x82\xd1\x8c,\xd0\xb1\xd0\xbb\xd1\x8f\xd0\xb4\xd1\x8c', b'\xd1\x85\xd1\x83\xd0\xb9', b'\xd0\xbf\xd0\xb8\xd0\xb7\xd0\xb4\xd0\xb0', b'\xd0\xbc\xd0\xb0\xd0\xbd\xd0\xb4\xd0\xb0', b'\xd0\xb4\xd1\x80\xd0\xbe\xd1\x87\xd0\xb8\xd1\x82\xd1\x8c', b'\xd0\xbf\xd0\xb8\xd0\xb4\xd0\xb0\xd1\x80\xd0\xb0\xd1\x81', b'\xd0\xb3\xd0\xb0\xd0\xbd\xd0\xb4\xd0\xbe\xd0\xbd')
		resp = "Все говорят \"" + msg.text + "\", а ты купи слона."
		if(time.localtime()[3] == 13): # since 13 to 14 o'c every day
			resp = "Извините, у меня обед. Боты тоже хотят отдыхать"
		elif any((trigger in msg.text.encode()) for trigger in kurwas):
			resp = "аккуратнее с языком"
		elif command in ("/init", "/help", "/test", "/intro", "алло"):
			#https://lurkmore.to/Абдуловера
			resp = "Ты биборан читал? Или ты масленок?"
		elif "слава украине" in msg.text.lower():
			resp = "Героям слава!"
		elif "цп" in msg.text.lower() or "наркотики" in msg.text.lower() or	"tor" in msg.text.lower() or "vpn" in msg.text.lower():
			resp = """Хорошая попытка, товарищ майор!"""
		elif "kde" in msg.text.lower() or "кеды" in msg.text.lower():
			#http://lurkmore.to/Лор
			resp = "Плазма не падает!"
		elif command.lower() in ("линукс", "гну", "linux", "gnu"):
			resp = "Теперь-то точно вендекапец!"
		elif command.lower() in ("/pb", "вероятность"):
			percent = 0
			for i in msg.text:
				percent += ord(i)
			resp = "Вероятность равна " + str(percent%100) + "%!"
		elif "Правда, что" in msg.text:
			resp = "Да, " + " ".join(msg.text[:-1].split(" ")[2:]) if len(msg.text) % 2 else "Нет, не Правда"
		elif "беспорядки" in msg.text.lower():
			resp = "осталось " + str(datetime.date(2018, 3, 18) - datetime.date.today()).split()[0] + " дней"
		self.send(resp, msg.id)


# t.me/infosecspbrubot
bot = Bot("464871138:AAEGxm_kqV9iN4haD71SL7uHCCxNpIDSyQU")
hndl = bot.handle(bot.onMessageGet)
next(hndl)